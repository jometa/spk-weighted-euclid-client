import axios from "./axios";
import { Kasus, ClassificationInput, ClassificationResult } from "@/interfaces";

export async function classify(payload: ClassificationInput): Promise<any> {
  const resp = await axios.post("/api/v1/classification", payload);
  const result: any = resp.data;
  return result;
}