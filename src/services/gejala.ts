import axios from "./axios";
import { Gejala } from "@/interfaces";

export async function gejalaBySexAndKeyword(woman: boolean, keyword: string = "") : Promise<Gejala[]> {
  const resp = await axios.get("/api/v1/gejala-view/sex-keyword", {
    params: {
      sex: woman ? 'woman' : 'man',
      keyword
    }
  });
  const items = (resp.data && Array.isArray(resp.data)) ? resp.data : [];
  return items as Gejala[];
}