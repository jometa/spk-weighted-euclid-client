import axios from "./axios";
import { RekamMedikCreateDTO } from "@/interfaces";
import evbus from "@/evbus";

export const createRekamMedik = (payload: RekamMedikCreateDTO) => {
  return axios
    .post("/api/v1/rekam-medik/", payload)
    .then((resp) => {
      evbus.$emit('repo-event', {
        type: 'success',
        msg: `sukses menambah data Rekam Medik`
      })
      return parseInt(resp.data);
    })
    .catch(err => {
      evbus.$emit('repo-event', {
        type: 'success',
        msg: `gagal menambah data Rekam Medik`
      })
      throw err
    });
};

// PATCH Method.
const patchRm = (res: string, method: 'post' | 'delete' | 'patch', id: number, idRes: any) => {
  const url = `/api/v1/rekam-medik/${id}/${res}/${idRes}`;
  switch (method) {
    case 'post':
      return axios.post(url);
    case 'delete':
      return axios.delete(url);
    case 'patch':
      return axios.patch(url);
    default:
      throw new Error('Unknown method: ' + method);
  }
};

export const updatePasien = (id: number, idRes: number) => patchRm('pasien', 'patch', id, idRes);
export const updatePenyakit = (id: number, idRes: number) => patchRm('penyakit', 'patch', id, idRes);
export const addGejala = (id: number, idRes: number) => patchRm('gejala', 'post', id, idRes);
export const removeGejala = (id: number, idRes: number) => patchRm('gejala', 'delete', id, idRes);
export const updateTanggal = (id: number, tanggal: string) => patchRm('tanggal', 'patch', id, tanggal);
