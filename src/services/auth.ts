const DEF_USERNAME = 'admin';
const DEF_PASSWORD = 'admin';
const KEY_USERNAME = 'derin.username'

export function login(username: string, password: string) {
    if (username != DEF_USERNAME) {
        throw new Error(`Wrong Username`);
    }
    if (password != DEF_PASSWORD) {
        throw new Error(`Wrong password`);
    }

    localStorage.setItem('derin.username', username);
}

export function checkUser() {
    const username = localStorage.getItem(KEY_USERNAME);
    if (username === null) {
        return false;
    }
    return username;
}

export function logout() {
    localStorage.removeItem(KEY_USERNAME);
}
