import axios from "./axios";
import { Penyakit } from "@/interfaces";

export async function penyakitBySexAndKeyword(woman: boolean, keyword: string = "") : Promise<Penyakit[]> {
  const resp = await axios.get("/api/v1/penyakit-view/sex-keyword", {
    params: {
      sex: woman ? 'woman' : 'man',
      keyword
    }
  });
  const items = (resp.data && Array.isArray(resp.data)) ? resp.data : [];
  return items as Penyakit[];
}