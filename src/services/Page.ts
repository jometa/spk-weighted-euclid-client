export interface Pageable {
  'number': number
  offset: number
  size: number
  sort: any
  sorted: boolean
}

export interface Page<T> {
  content: T[]
  empty: Boolean
  numberOfElements: number
  offset: number
  pageNumber: number
  size: number
  totalPages: number
  totalSize: number
}