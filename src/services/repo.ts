import axios from "./axios";
import { Page } from "./Page";
import { Penyakit, Gejala, Pasien, RekamMedik, Option, KasusDTO } from "@/interfaces";
import evbus, { RepoEvent } from "@/evbus";

type IOptTransformer<T> = (s: T) => Option;

export class HttpRepo<T> {
  constructor (
    private readonly baseUrl: string,
    private readonly name: string
  ) {}

  async findAll(from: number, size: number) : Promise<Page<T>> {
    const params = { from, size }
    return axios.get(this.baseUrl, { params })
      .then(resp => resp.data as Page<T>)
  }

  async findOne(id: number) : Promise<T> {
    return axios.get(this.baseUrl + '/' + id)
      .then(resp => resp.data as T)
  }

  async create(payload: T) : Promise<void> {
    return axios.post(this.baseUrl + "/", payload)
      .then(resp => resp.data as T)
      .then(() => {
        evbus.$emit('repo-event', {
          type: 'success',
          msg: `sukses menambah data ${this.name}`
        })
      })
      .catch(err => {
        evbus.$emit('repo-event', {
          type: 'success',
          msg: `gagal menambah data ${this.name}`
        })
        throw err
      })
  }

  async update(id: number, payload: T): Promise<void> {
    return axios.put(this.baseUrl + '/' + id, payload)
      .then(() => {
        evbus.$emit('repo-event', {
          type: 'success',
          msg: `sukses mengubah data ${this.name}`
        })
      })
      .catch(err => {
        evbus.$emit('repo-event', {
          type: 'success',
          msg: `gagal mengubah data ${this.name}`
        })
        throw err
      })
  }

  async remove(id: number) : Promise<void> {
    return axios.delete(this.baseUrl + "/" + id)
      .then(resp => {
        evbus.$emit('repo-event', {
          type: 'success',
          msg: `sukses menghapus data ${this.name}`
        })
      })
      .catch(err => {
        evbus.$emit('repo-event', {
          type: 'success',
          msg: `gagal menghapus data ${this.name}`
        })
        throw err
      })
  }

  async options(transform: IOptTransformer<T>): Promise<Option[]> {
    return this.findAll(0, 100000)
      .then(page => page.content.map(transform))
  }
}

export const GejalaRepo = new HttpRepo<Gejala>("/api/v1/gejala", "Gejala");
export const PenyakitRepo = new HttpRepo<Penyakit>("/api/v1/penyakit", "Penyakit");
export const PasienRepo = new HttpRepo<Pasien>("/api/v1/pasien", "Pasien");
export const RekamMedikRepo = new HttpRepo<RekamMedik>("/api/v1/rekam-medik", "Rekam Medik");
export const KasusRepo = new HttpRepo<KasusDTO>("/api/v1/basis-kasus", "Kasus");
