// Implement session with localStorage

function fkey(key: string) {
  return `derin-cbr-app:${key}`;
}

export function saveSession(key: string, payload: any) {
  localStorage.setItem(fkey(key), JSON.stringify(payload));
}

export function getSession(key: string): any {
  const item = localStorage.getItem(fkey(key));
  if (item == null) {
    return null;
  } else {
    console.log(item);
    return JSON.parse(item);
  }
}