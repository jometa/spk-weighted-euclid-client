import axios from "./axios";
import { Penyakit } from "@/interfaces";

type PCount = [Penyakit, number];

export async function penyakitCountInBasisKasus(): Promise<PCount[]> {
  const response = await axios.get("/api/v1/aggregate/penyakit/basiskasus/count");
  const items: [Penyakit, number][] = response.data.map((it: any) => ([
    it._0,
    it._1
  ]));
  return items;
};