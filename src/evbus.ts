import Vue from "vue";

const evbus = new Vue();

export type RepoEvent = {
  type: string;
  msg: string;
};

export default evbus;