export default {
  tableHeaders: [
    {
      text: '#',
      value: 'id'
    },
    {
      text: 'Tanggal',
      value: 'tanggal'
    },
    {
      text: 'Penyakit',
      value: 'penyakit'
    },
    {
      text: 'Pasien',
      value: 'pasien'
    },
    {
      text: 'Basis Kasus',
      value: 'basisKasus'
    },
    {
      text: '',
      value: 'action'
    }
  ],
  defaultData: {
    tanggal: new Date(),
    basisKasus: false,
    idPenyakit: undefined,
    listIdGejala: [],
    idPasien: undefined
  }
}