export default {
  tableHeaders: [
    {
      text: '#',
      value: 'id'
    },
    {
      text: 'Nama',
      value: 'nama'
    },
    {
      text: 'Deskripsi',
      value: 'deskripsi'
    },
    {
      text: 'Solusi',
      value: 'solusi'
    },
    {
      text: 'Pria',
      value: 'man'
    },
    {
      text: 'Wanita',
      value: 'woman'
    },
    {
      text: '',
      sortable: false,
      value: 'action'
    }
  ],
  defaultData: {
    nama: '',
    deskripsi: '',
    solusi: '',
    man: true,
    woman: true
  }
}
