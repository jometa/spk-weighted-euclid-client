export default {
  tableHeaders: [
    {
      text: '#',
      value: 'id'
    },
    {
      text: 'Nama',
      value: 'nama'
    },
    {
      text: 'Deskripsi',
      value: 'deskripsi'
    },
    {
      text: 'Pria',
      value: 'man'
    },
    {
      text: 'Wanita',
      value: 'woman'
    },
    {
      text: '',
      sortable: false,
      value: 'action'
    }
  ],
  defaultData: {
    nama: '',
    deskripsi: '',
    woman: true,
    man: true
  }
}
