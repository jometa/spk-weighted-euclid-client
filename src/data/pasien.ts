export default {
  tableHeaders: [
    {
      text: '#',
      value: 'id'
    },
    {
      text: 'Nama',
      value: 'nama'
    },
    {
      text: 'Sex',
      value: 'woman'
    },
    {
      text: 'TTL',
      value: 'ttl'
    },
    {
      text: 'Alamat',
      value: 'alamat'
    },
    {
      text: '',
      sortable: false,
      value: 'action'
    }
  ],
  defaultData: {
    nama: 'Degan',
    woman: false,
    tanggalLahir: new Date('1994-04-11'),
    tempatLahir: 'Hutan Camplong',
    alamat: 'Oekabiti'
  }
}
