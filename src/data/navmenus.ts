export default {
  master: [
    {
      text: 'Penyakit',
      icon: 'credit-card',
      color: 'pink',
      to: '/app/penyakit/list'
    },
    {
      text: 'Gejala',
      icon: 'card-text',
      color: 'teal',
      to: '/app/gejala'
    },
    {
      text: 'Rekam Medik',
      icon: 'book-multiple',
      color: 'blue lighten-1',
      to: '/app/rekam-medik'
    },
    {
      text: 'Kasus',
      icon: 'hiking',
      color: 'indigo',
      to: '/app/kasus'
    },
    {
      text: 'Pasien',
      icon: 'hiking',
      color: 'gold',
      to: '/app/pasien'
    }
  ],
  testing: [
    {
      text: 'Test',
      icon: 'lightbulb-outline',
      color: 'brown',
      to: '/app/testing/single'
    },
    {
      text: 'K-Fold',
      icon: 'lightbulb-on',
      color: 'brown',
      to: '/app/testing/dataset'
    }
  ]
}