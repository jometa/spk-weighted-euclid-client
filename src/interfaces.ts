export interface Penyakit {
  id?: number
  nama: string
  deskripsi: string
  solusi: string
  woman: boolean
  man: boolean
}

export interface Gejala {
  id?: number
  nama: string
  deskripsi: string
  woman: boolean
  man: boolean
}

export interface Pasien {
  id?: number
  nama: string
  woman: boolean
  tanggalLahir: Date
  tempatLahir: string
  alamat: string
}

export interface ExtPasien extends Pasien {
  age: number;
}

export interface RekamMedikCreateDTO {
  tanggal: string;
  idPasien: number;
  idPenyakit: number;
  idListGejala: number[];
  basisKasus: boolean;
}

export interface RekamMedik {
  id?: number;
  penyakit?: Penyakit;
  gejalas?: Gejala[];
  pasien?: Pasien;
  tanggal: string;
}

export interface Option {
  value: number;
  text: string;
}

export interface KasusDTO {
  id: number;
  penyakitId: number;
  penyakitNama: string;
  rekamMedikId: number;
  type: string;
}

export interface Kasus {
  id: number;
  rekamMedik?: RekamMedik;
  similarity?: number;
  type: string;
}

export interface ClassificationInput {
  nama: string;
  woman: boolean;
  alamat: string;
  tanggalLahir: string;
  tempatLahir: string;
  idListGejala: number[];
};

export type ClassificationResult = [Kasus, number];