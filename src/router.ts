import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "about" */ './views/Home.vue')
    },
    {
      path: '/app',
      name: 'app',
      component: () => import(/* webpackChunkName: "about" */ './views/LoginWrapper.vue'),
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: () => import(/* webpackChunkName: "about" */ './views/app/dashboard/index.vue')
        },
        {
          path: 'penyakit',
          name: 'penyakit',
          component: () => import("./views/app/penyakit/index.vue"),
          children: [
            {
              path: 'list',
              name: 'penyakit-list',
              component: () => import("./views/app/penyakit/list.vue")
            },
            {
              path: 'add',
              name: 'penyakit-add',
              component: () => import("./views/app/penyakit/add.vue")
            },
            {
              path: ':id',
              props: true,
              name: 'penyakit-add',
              component: () => import("./views/app/penyakit/edit.vue")
            }
          ]
        },
        {
          path: 'gejala',
          name: 'gejala',
          component: () => import("./views/app/gejala/index.vue"),
          redirect: '/app/gejala/list',
          children: [
            {
              path: 'list',
              name: 'gejala-list',
              component: () => import("./views/app/gejala/list.vue")
            },
            {
              path: 'add',
              name: 'gejala-add',
              component: () => import("./views/app/gejala/add.vue")
            },
            {
              path: ':id',
              props: true,
              name: 'gejala-add',
              component: () => import("./views/app/gejala/edit.vue")
            }
          ]
        },
        {
          path: 'pasien',
          name: 'pasien',
          component: () => import("./views/app/pasien/index.vue"),
          redirect: '/app/pasien/list',
          children: [
            {
              path: 'list',
              name: 'pasien-list',
              component: () => import("./views/app/pasien/list.vue")
            },
            {
              path: 'add',
              name: 'pasien-add',
              component: () => import("./views/app/pasien/add.vue")
            },
            {
              path: ':id',
              props: true,
              name: 'pasien-add',
              component: () => import("./views/app/pasien/edit.vue")
            }
          ]
        },
        {
          path: 'rekam-medik',
          name: 'rekam-medik',
          component: () => import("./views/app/rekam-medik/index.vue"),
          redirect: '/app/rekam-medik/list',
          children: [
            {
              path: 'list',
              name: 'rekam-medik-list',
              component: () => import("./views/app/rekam-medik/list.vue"),
            },
            {
              path: 'add',
              name: 'rekam-medik-add',
              component: () => import("./views/app/rekam-medik/add.vue"),
            },
            {
              path: 'detail/:id',
              props: true,
              name: 'rekam-medik-detail',
              component: () => import("./views/app/rekam-medik/detail.vue")
            },
            {
              path: 'patch/pasien',
              props: route => ({ ...route.query }),
              component: () => import("./views/app/rekam-medik/patch/pasien.vue")
            },
            {
              path: 'patch/gejala',
              props: route => ({ ...route.query }),
              component: () => import("./views/app/rekam-medik/patch/gejala.vue")
            },
            {
              path: 'patch/penyakit',
              props: route => ({ ...route.query }),
              component: () => import("./views/app/rekam-medik/patch/penyakit.vue")
            }
          ]
        },
        {
          path: 'kasus',
          component: () => import("./views/app/kasus/index.vue"),
          redirect: "/app/kasus/list",
          children: [
            {
              path: "list",
              component: () => import("./views/app/kasus/list.vue")
            }
          ]
        },
        {
          path: 'testing/dataset',
          component: () => import("./views/app/test/dataset.vue")
        },
        {
          path: 'testing/dataset-result',
          component: () => import("./views/app/test/dataset-result.vue")
        },
        {
          path: 'testing/single',
          component: () => import("./views/app/test/single.vue")
        },
        {
          path: 'testing/single-result',
          component: () => import("./views/app/test/single-result.vue")
        },
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
