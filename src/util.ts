export function age(date: Date): number {
  let diffMs = Date.now() - date.getTime();
  let ageDate = new Date(diffMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}
